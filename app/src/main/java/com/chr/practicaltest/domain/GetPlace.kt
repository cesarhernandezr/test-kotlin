package com.chr.practicaltest.domain

import com.chr.practicaltest.data.PlaceRepository
import javax.inject.Inject

class GetPlace @Inject constructor(
    private val repository: PlaceRepository
) {
    suspend operator fun invoke() = repository.getPlace()
}