package com.chr.practicaltest.domain

import com.chr.practicaltest.data.PlaceRepository
import javax.inject.Inject

class GetPlaces @Inject constructor(
    private val repository: PlaceRepository
) {
    operator fun invoke() = repository.getPlaces()
}