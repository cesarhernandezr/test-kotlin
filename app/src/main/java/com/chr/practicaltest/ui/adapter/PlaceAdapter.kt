package com.chr.practicaltest.ui.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chr.practicaltest.R
import com.chr.practicaltest.core.inflate
import com.chr.practicaltest.data.model.Place
import com.chr.practicaltest.databinding.ItemPlaceBinding
import com.chr.practicaltest.ui.listener.RecyclerPlaceListener

class PlaceAdapter(
    private val items: List<Place>,
    private val listener: RecyclerPlaceListener
): RecyclerView.Adapter<RecyclerView.ViewHolder>()
{
    private val layout = R.layout.item_place

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder
    {
        return ViewHolder(parent.inflate(layout))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int)
    {
        (holder as ViewHolder).bind(items[position], listener)
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    {
        private val binding = ItemPlaceBinding.bind(itemView)
        fun bind(place: Place, listener: RecyclerPlaceListener) = with(itemView)
        {
            binding.textViewName.text = place.name
            binding.textViewCoordinates.text = resources.getString(
                R.string.coordinates,
                place.coordinates.latitude,
                place.coordinates.longitude)
            setOnClickListener { listener.onClickPlace(place, adapterPosition) }
        }
    }
}