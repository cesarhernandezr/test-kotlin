package com.chr.practicaltest.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chr.practicaltest.domain.GetPlace
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RequestViewModel @Inject constructor(
    private val getPlace: GetPlace
): ViewModel() {
    val isLoading = MutableLiveData<Boolean>()

    fun onCreate()
    {
        viewModelScope.launch {
            isLoading.postValue(true)
            getPlace()
            isLoading.postValue(false)
        }
    }
}