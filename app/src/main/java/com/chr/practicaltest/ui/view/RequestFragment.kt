package com.chr.practicaltest.ui.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.chr.practicaltest.databinding.FragmentRequestBinding
import com.chr.practicaltest.ui.viewmodel.RequestViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RequestFragment: Fragment()
{
    private var _binding: FragmentRequestBinding? = null
    private val binding get() = _binding!!
    private val requestViewModel: RequestViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRequestBinding.inflate(inflater, container, false)
        val view = binding.root

        requestViewModel.onCreate()
        setupObservers()
        setListeners()

        return view
    }

    private fun setListeners()
    {
        binding.buttonExecuteRequest.setOnClickListener { requestViewModel.onCreate() }
    }

    private fun setupObservers()
    {
        requestViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            binding.loading.isVisible = it
            binding.buttonExecuteRequest.isActivated = it
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}