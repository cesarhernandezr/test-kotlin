package com.chr.practicaltest.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chr.practicaltest.data.model.Place
import com.chr.practicaltest.domain.GetPlace
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MapsViewModel @Inject constructor(
    private val getPlace: GetPlace
): ViewModel() {
    val placeModel = MutableLiveData<Place>()

    fun onCreate()
    {
        viewModelScope.launch {
            val place = getPlace()

            if (place != null) {
                placeModel.postValue(place!!)
            }
        }
    }
}