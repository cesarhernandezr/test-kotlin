package com.chr.practicaltest.ui.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.chr.practicaltest.R
import com.chr.practicaltest.databinding.FragmentMapsViewBinding
import com.chr.practicaltest.ui.viewmodel.MapsViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MapsViewFragment : Fragment(), OnMapReadyCallback
{
    private var _binding: FragmentMapsViewBinding? = null
    private val binding get() = _binding!!
    private val mapsViewModel: MapsViewModel by viewModels()
    private lateinit var map: GoogleMap

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMapsViewBinding.inflate(inflater, container, false)
        val view = binding.root
        mapsViewModel.onCreate()
        createMapFragment()
        return view
    }

    private fun createMapFragment()
    {
        val mapFragment = childFragmentManager.findFragmentById(R.id.fragmentMap) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun createMarker()
    {
        mapsViewModel.placeModel.observe(viewLifecycleOwner, Observer {
            if (!it.coordinates.latitude.isNullOrEmpty() && !it.coordinates.longitude.isNullOrEmpty()) {
                val markerLocation = LatLng(it.coordinates.latitude.toDouble(), it.coordinates.longitude.toDouble())
                map.addMarker(MarkerOptions().position(markerLocation).title("Place"))
                map.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(markerLocation, 18f),
                    4000,
                    null
                )
            }
        })
    }

    override fun onMapReady(googleMap: GoogleMap)
    {
        map = googleMap
        createMarker()
    }
}