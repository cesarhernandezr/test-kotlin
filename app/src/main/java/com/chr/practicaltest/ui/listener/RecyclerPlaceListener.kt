package com.chr.practicaltest.ui.listener

import com.chr.practicaltest.data.model.Place

interface RecyclerPlaceListener
{
    fun onClickPlace(place: Place, position: Int)
}