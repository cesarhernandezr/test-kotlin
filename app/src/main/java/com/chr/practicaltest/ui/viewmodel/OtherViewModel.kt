package com.chr.practicaltest.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.chr.practicaltest.domain.GetPlaces
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class OtherViewModel @Inject constructor(
    getPlaces: GetPlaces
): ViewModel() {
    val places = getPlaces()
}