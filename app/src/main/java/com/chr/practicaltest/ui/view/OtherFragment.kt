package com.chr.practicaltest.ui.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.chr.practicaltest.R
import com.chr.practicaltest.data.model.Place
import com.chr.practicaltest.databinding.FragmentOtherBinding
import com.chr.practicaltest.ui.adapter.PlaceAdapter
import com.chr.practicaltest.ui.listener.RecyclerPlaceListener
import com.chr.practicaltest.ui.viewmodel.OtherViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OtherFragment: Fragment(), RecyclerPlaceListener
{
    private var _binding: FragmentOtherBinding? = null
    private val binding get() = _binding!!
    private val otherViewModel: OtherViewModel by viewModels()

    private lateinit var adapter: PlaceAdapter
    private val placeList: ArrayList<Place> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentOtherBinding.inflate(inflater, container, false)
        val view = binding.root
        setUpRecyclerView()
        setupObservers()
        return view
    }

    private fun setUpRecyclerView()
    {
        val layoutManager = LinearLayoutManager(context)
        adapter = PlaceAdapter(placeList, this)

        binding.recyclerView.setHasFixedSize(true)
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.itemAnimator = DefaultItemAnimator()
        binding.recyclerView.adapter = adapter
    }

    private fun setupObservers()
    {
        otherViewModel.places.observe(viewLifecycleOwner, Observer {
            placeList.clear()
            placeList.addAll(it)
            adapter.notifyDataSetChanged()
        })
    }

    override fun onClickPlace(place: Place, position: Int)
    {
        Toast.makeText(requireContext(),
            resources.getString(
                R.string.coordinates,
                place.coordinates.latitude,
                place.coordinates.longitude),
            Toast.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}