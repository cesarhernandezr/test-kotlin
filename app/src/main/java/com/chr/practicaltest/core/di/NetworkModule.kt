package com.chr.practicaltest.core.di

import android.content.Context
import com.chr.practicaltest.data.local.AppDatabase
import com.chr.practicaltest.data.local.PlaceDao
import com.chr.practicaltest.data.network.PlaceApiClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule
{
    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit
    {
        return Retrofit.Builder()
            .baseUrl("https://community-open-weather-map.p.rapidapi.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun providePlaceApiClient(retrofit: Retrofit): PlaceApiClient
    {
        return retrofit.create(PlaceApiClient::class.java)
    }

    @Singleton
    @Provides
    fun providePlaceDao(db: AppDatabase): PlaceDao = db.placeDao()

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context):
            AppDatabase = AppDatabase.getDatabase(appContext)
}