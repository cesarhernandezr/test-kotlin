package com.chr.practicaltest.data.model

import com.google.gson.annotations.SerializedName

data class Coordinate(
    @SerializedName("lat") val latitude: String? = null,
    @SerializedName("lon") val longitude: String? = null
)
