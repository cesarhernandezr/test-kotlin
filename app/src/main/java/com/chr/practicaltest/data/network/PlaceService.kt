package com.chr.practicaltest.data.network

import com.chr.practicaltest.data.model.Place
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PlaceService @Inject constructor(
    private val api: PlaceApiClient
) {
    suspend fun getPlace(): Place?
    {
        return withContext(Dispatchers.IO) {
            val response = api.getPlace()
            response.body()
        }
    }
}