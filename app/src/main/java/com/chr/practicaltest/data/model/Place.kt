package com.chr.practicaltest.data.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Place(
    @PrimaryKey(autoGenerate = true)
    val idLocal: Int,
    @SerializedName("id") val idResponse: Int,
    val name: String,
    @SerializedName("coord") @Embedded val coordinates: Coordinate
)
