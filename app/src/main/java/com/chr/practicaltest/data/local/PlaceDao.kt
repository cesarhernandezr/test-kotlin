package com.chr.practicaltest.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.chr.practicaltest.data.model.Place

@Dao
interface PlaceDao
{
    @Query("SELECT * from Place")
    fun getAllPlaces() : LiveData<List<Place>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(place: Place)
}