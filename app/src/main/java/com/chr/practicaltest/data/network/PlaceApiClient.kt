package com.chr.practicaltest.data.network

import com.chr.practicaltest.data.model.Place
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface PlaceApiClient
{
    @GET("weather")
    suspend fun getPlace(
        @Header("x-rapidapi-key")
        apiKey: String = "d3fe3a34c5mshf70e1bb90c464a8p1ba4aajsn4233972e8a9c",
        @Query("q") query: String = "London"
    ): Response<Place>
}