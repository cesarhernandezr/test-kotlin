package com.chr.practicaltest.data

import androidx.lifecycle.LiveData
import com.chr.practicaltest.data.local.PlaceDao
import com.chr.practicaltest.data.model.Place
import com.chr.practicaltest.data.network.PlaceService
import javax.inject.Inject

class PlaceRepository @Inject constructor(
    private val api: PlaceService,
    private val localDataSource: PlaceDao
) {
    suspend fun getPlace(): Place?
    {
        val response = api.getPlace()
        localDataSource.insert(response!!)
        return response
    }

    fun getPlaces(): LiveData<List<Place>>
    {
        return localDataSource.getAllPlaces()
    }
}