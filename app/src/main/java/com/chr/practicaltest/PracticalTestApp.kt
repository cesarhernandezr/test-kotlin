package com.chr.practicaltest

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PracticalTestApp: Application()
